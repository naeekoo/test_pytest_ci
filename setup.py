#!/usr/bin/python
# Licensed under a 3-clause BSD style license - see LICENSE.rst
# -*-coding:Utf-8 -*

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

#with open('LICENSE') as f:
#    license = f.read()

setup(
    name='math38012',
    version='1.0.1',
    description='Package to run querries on SMS Databases',
    long_description=readme,
    author='Inès Verrier',
    author_email='ines.verrier@sharemyspace.space',
    url='git@gitlab.sms.local:calm/execute_query.git',
    license='license',
    packages=find_packages()
)

